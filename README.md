# ics-ans-logo

Ansible role to install node-red and control LOGOs (PLCs) with Nginx as a reverse proxy for authentication and authorization.

Node-RED is a flow-based development tool for visual programming.

LOGOs are logic modules from Siemens



## License

BSD 2-clause
