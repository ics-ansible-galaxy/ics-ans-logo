import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("logo")


def test_node_red_container_is_running(host):
    with host.sudo():
        node_red = host.docker("node-red-logo")
        assert node_red.is_running


def test_nginx_proxy_container_is_running(host):
    with host.sudo():
        node_red = host.docker("nginx-proxy")
        assert node_red.is_running
